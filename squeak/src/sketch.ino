#include "pitches.h"
const int PIEZO_PIN = 8;

int melody[] = {NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_G4, NOTE_B5, NOTE_C5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_C5, NOTE_B5, NOTE_A5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_G4, NOTE_B5, NOTE_C5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_C5, NOTE_B5, NOTE_A5, NOTE_B5, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_G4, NOTE_F5, NOTE_E5, NOTE_D5, NOTE_F4, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_E4, NOTE_D5, NOTE_C5, NOTE_B5, NOTE_E4, NOTE_E5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_G4, NOTE_B5, NOTE_C5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_C5, NOTE_B5, NOTE_A5 };

int beats[] = { 1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,2,1,1,1,2,1,1,1,2,1,1,1,3,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,6};

int tempo = 175;
int note_count;

void setup() {
  note_count = sizeof(melody);
}

void loop() {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < note_count; thisNote++) {
    int noteDuration = beats[thisNote] * tempo;
    tone(PIEZO_PIN, melody[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(PIEZO_PIN);
  }
}

