<?php
/*

  php pregcode.php
228.57142857143
320


  m12 bolt - pitch is 1.75/400 = 1.6 + .15 = .4 +
  m8 bolt - pitch is 1.25


   */

//echo 400/1.75;
//echo 400/1.25;
//exit;

test();

/**
 * Read a line from a specified resource
 */
function getLine($file, $time_limit) {
  $start = time();
  $result = '';
  do {
    $char = fgetc($file);
    if ($char !== false) {
      $result .= $char;
    }
    $runtime = time() - $start;
  } while (($runtime < $time_limit) && $char != "\n");
  //    echo "Got (in $runtime s): " . json_encode(array($result)) . "\n";
  $result = trim($result);
  echo "<< $result\n";
  return $result;
}

function arduinoFile() {
  $path = "/dev/ttyUSB0";
  $file =fopen($path, "w+");
  if (!$file) {
    $path = "/dev/ttyACM0";
    $file =fopen($path, "w+");
  }
  if ( $file) {
    echo "-- talking with Arduino at $path\n";
    return $file;
  }
  else {
    throw new Exception('bad file');
  }
}


function waitFor($file, $regex) {
  $reply = '';
  $count = 0;
  while (!preg_match($regex, $reply)) {
    if ($count == 10) {
      echo "-- waiting for $regex\n";
    }
    $reply = getLine($file, 100);
    $count ++;
  }
}

function writeData($file, $data) {
  foreach ($data AS $line) {
    fwrite($file, "$line\n");
    echo ">> $line\n";
    waitFor($file, '/^ok$/');
  }
}


function test() {
  $data = array(
      'J50', // go to joystick mode, 5sec timeout
      'Z', // zero all axes
      'G 0, 0, 8, 40;',
      'G 10, 10, 8, 40;',
      // outer square:
      'G 10, 10, -5, 40;', //plunge
      'G 50, 10, -5, 40;', //
      'G 50, 50, -5, 40;', //
      'G 10, 50, -5, 40;', //
      'G 10, 10, -5, 40;', //
      'G 10, 10, 8, 40;',// up
      'G 20, 20, 8, 40;',// up
      // inner sqare:
      'G 20, 20, -5, 40;', //plunge
      'G 40, 20, -5, 40;', //
      'G 40, 40, -5, 40;', //
      'G 20, 40, -5, 40;', //
      'G 20, 20, -5, 40;', //
      'G 20, 20, 8, 40;',// up
      'G 0, 0, 8, 40;', // home
//      'G 1, 2, 3, 40;',
//      'G 1, 4, 2, 40;',
//      'G 2, 4, 2, 40;',
      );
  $file = arduinoFile();
  echo "-- waiting for ok to start\n";
  waitFor($file, '/^ok$/');
  echo "-- starting upload\n";
  writeData($file, $data);
  fclose($file);
}

exit;

$finish = array('X' => 0, 'Y' =>0, 'Z'=> 0, 'F' => 0);
// s=o/h c=a/h t=o/a
//
//
//                  # p                    90
//                 /|                       |
//                / |                  180--*-- 0
//   h (radius)  /  |  o ( Yc - Yp)         |
//              /)  |                      270
//      centre #----#
//               a (Xc- Xp)
//
//

function angle($centre, $point, $radians = false) {
  return atan2(
    $centre['Y'] + $point['Y'],
    $centre['X'] + $point['X']
  ) * ($radians ? 1 : 180 / M_PI);
}

function point_on_circle($centre, $radius, $angle, $radians = false) {
  $decimals = 3;
  return array(
    'X' => round($centre['X'] + $radius * cos($angle / ($radians ? 1 : 180 / M_PI)), $decimals),
    'Y' => round($centre['Y'] + $radius * sin($angle / ($radians ? 1 : 180 / M_PI)), $decimals),
  );
}

function command($params) {
  echo "G$params[G] X$params[X] Y$params[Y] Z$params[Z] F$params[F]\n";
}

function sign($number) {
    return $number / abs($number ?: 1);
}

// M2 - prorgram end
// M3 - spindle on
// M5 - spindle off
// G00 - rapid linear move (F = feed rate)
// G01 - slow linear move
// G02 - clock wise
// G03 - counter clockwise
// G21 - all units in millimeters

while($line = fgets(STDIN)){
  // strip bracketed comments:
  $clean_line = trim(preg_replace("/\([^)]+\)/", "", $line));
  // switch on command first char:
  switch (substr($clean_line, 0, 1)) {
    case 'G':
    // split line by whitespace
    $parts = preg_split('/\s+/', $clean_line);
    if ($parts) {
      $params = array();
      foreach ($parts AS $part) {
        $i = substr($part, 0, 1);
        $params[$i] = round((real)substr($part, 1),2);
      }
      // fill in unspecified params from last finish:
      $params += $finish;
      $start = $finish;
      // update finish:
      foreach ($finish AS $i => $value) {
        $finish[$i] = $params[$i];
      }
//      echo json_encode($params) . "\n";
      $gmode = $params['G'];
      // handle curves
      if ($gmode == 2 or $gmode == 3) {
        $params['G'] = 1; // go fast linean
        $radius = sqrt($params['I']*$params['I']+$params['J']*$params['J']);
        $centre = array(
          'X' => $start['X'] + $params['I'],
          'Y' => $start['Y'] + $params['J'],
        );
        $start_angle = angle($centre, $start);
        $finish_angle = angle($centre, $finish);
        // angle step is proportional to radius
        // G02 - clock wise
        // G03 - counter clockwise
        $angle_step = ($gmode == 2 ? -1 : 1) * 100 / $radius; // in degrees
        $angle_delta = $finish_angle - $start_angle;
        // adjust for if crossing the 0 angle point
        if (sign($angle_step) != sign($angle_delta)) {
          $finish_angle += 360 * sign($angle_step);
          $angle_delta = $finish_angle - $start_angle;
        }
        $steps = floor($angle_delta / $angle_step);
        $debug = "start_angle  = $start_angle finish_angle  = $finish_angle
          radius = $radius
          start = $start[X],$start[Y]
          finish = $finish[X],$finish[Y]
          angle_step = $angle_step
          angle_delta = $angle_delta
          steps = $steps
          centre = $centre[X],$centre[Y]
          ";
        // echo $debug;
        for ($s = 1; $s < $steps; $s++) {
          $angle = $start_angle + $s*$angle_step;
          // echo "angle: $angle (step $s)\n";
          $interim_point = point_on_circle($centre, $radius, $angle) + $params;
          command($interim_point);
        }
      }
      command($params);
    }
    break;
    case 'M':
      echo $clean_line;
  }
}





/* Test:

  echo "
  G1 X0 Y10
  G2 X0 Y-10 I0 J-10 (clockwise down)
  G3 X0 Y10 I0 J10 (counter-clockwise up)
  " | php pregcode.php

ls



G1 X0 Y10 Z0 F0
G1 X1.736 Y9.848 Z0 F0
G1 X3.42 Y9.397 Z0 F0
G1 X5 Y8.66 Z0 F0
G1 X6.428 Y7.66 Z0 F0
G1 X7.66 Y6.428 Z0 F0
G1 X8.66 Y5 Z0 F0
G1 X9.397 Y3.42 Z0 F0
G1 X9.848 Y1.736 Z0 F0
G1 X10 Y0 Z0 F0
G1 X9.848 Y-1.736 Z0 F0
G1 X9.397 Y-3.42 Z0 F0
G1 X8.66 Y-5 Z0 F0
G1 X7.66 Y-6.428 Z0 F0
G1 X6.428 Y-7.66 Z0 F0
G1 X5 Y-8.66 Z0 F0
G1 X3.42 Y-9.397 Z0 F0
G1 X1.736 Y-9.848 Z0 F0
G1 X0 Y-10 Z0 F0
G1 X1.736 Y-9.848 Z0 F0
G1 X3.42 Y-9.397 Z0 F0
G1 X5 Y-8.66 Z0 F0
G1 X6.428 Y-7.66 Z0 F0
G1 X7.66 Y-6.428 Z0 F0
G1 X8.66 Y-5 Z0 F0
G1 X9.397 Y-3.42 Z0 F0
G1 X9.848 Y-1.736 Z0 F0
G1 X10 Y0 Z0 F0
G1 X9.848 Y1.736 Z0 F0
G1 X9.397 Y3.42 Z0 F0
G1 X8.66 Y5 Z0 F0
G1 X7.66 Y6.428 Z0 F0
G1 X6.428 Y7.66 Z0 F0
G1 X5 Y8.66 Z0 F0
G1 X3.42 Y9.397 Z0 F0
G1 X1.736 Y9.848 Z0 F0
G1 X0 Y10 Z0 F0


cat output_0002.ngc | php pregcode.php > f

mv output_0002.ngc raw.gcode

java -version
java version "1.6.0_35"
OpenJDK Runtime Environment (IcedTea6 1.13.7) (6b35-1.13.7-1ubuntu0.12.04.2)
OpenJDK 64-Bit Server VM (build 23.25-b01, mixed mode)

  echo "G1 F2 (blah)
  G1 X1 Y1.5555
  G0 Y1 Z2
  " | php pregcode.php

G1 X0 Y0 Z0 F2
G1 X1 Y1.56 Z0 F2
G0 X1 Y1 Z2 F2

 */



