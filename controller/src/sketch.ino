/*
   The Beast Controller - joystick and serial mover
 */

#define MIN_ANALOGUE_READ_GAP 200
#define REPORT_FREQUENCY 1000
#define X_CLICKS_PER_MM 228.5714 // M12 threads 400/1.75
#define Y_CLICKS_PER_MM 320 // M8 thread 400/1.25
#define Z_CLICKS_PER_MM 320

//  pins:

#define JOYSTICK_X_PIN 2
#define JOYSTICK_AXIS_SWITCH_PIN 4
#define JOYSTICK_Y_PIN 3
#define X_PIN 6
#define Y_PIN 8
#define Z_PIN 10

#define READ_COMMAND_MODE 'G'

int cycle_duration = 800; // in micorseconds
unsigned int joystick_inactivity_timeout = 50; // = 5 seconds
// steps from home (in exact steps):
long x = 0;
long y = 0;
long z = 0;
// record the position at start of cylce:
long start_x = x; long start_y = y; long start_z = z;
// these float values are for the desired exact placement
float path_x = 0;
float path_y = 0;
float path_z = 0;
long cycle = 0;
long target_x = 0; long target_y = 0; long target_z = 0;

long feed_rate = 0;

long move_steps = 0;

float delta_x = 0; float delta_y = 0; float delta_z = 0;

// the previous reading from the analog input
int joyX = 518;
int joyY = 518;
int joyZ = 518;
unsigned long last_analogue_read_time = 0;
unsigned long last_joystick_move_time = 0;
unsigned long last_notice_time = 0;
char last_analogue_read_axis = 'X';
char mode = READ_COMMAND_MODE; // joystick mode



void setup() {
  Serial.begin(9600);
  for (int i=6; i <= 13 ; i++){
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
  pinMode(JOYSTICK_AXIS_SWITCH_PIN, INPUT);
  digitalWrite(JOYSTICK_AXIS_SWITCH_PIN, HIGH);
  Serial.println("starting");
  report();
  if (mode == 'G') {
    reportReady();
  }
}

void loop() {
  cycle ++;
  unsigned long start_time = millis();
  // record starting position:
  start_x = x; start_y = y; start_z = z;
  //  Serial.print(".");
  switch (mode) {
    case 'J': // joystick mode
      {
        // attempt to get the sensor value
        update_joy();
        x += moveAxisJoy(joyX, x, X_PIN);
        y += moveAxisJoy(joyY, y, Y_PIN);
        z += moveAxisJoy(joyZ, z, Z_PIN);
        if (!weMoved()) {
          if (millis()-max(last_joystick_move_time, last_notice_time) >1000) {
            // start report at end of first second
            Serial.print(cycle);
            Serial.println("waiting for joystick");
            last_notice_time = millis();
          }
          if (millis()-last_joystick_move_time > joystick_inactivity_timeout * 100) {
            Serial.println("left joystick mode");
            mode = 'G'; // gcode mode
            reportReady();
          }
        }
        else {
          last_joystick_move_time = millis();
        }
      }
      break;
    case 'G': // serial gcode mode
      {
        if (Serial.available() > 0) {
          char command = Serial.read();
          switch (command) {
            case 'J':
              mode = 'J';
              joystick_inactivity_timeout = Serial.parseInt();
              Serial.print("joystick mode: timeout is ");
              Serial.print(joystick_inactivity_timeout * 100);
              Serial.println("ms");
              break;
            case 'G':
              readGCommand();
              break;
            case 'Z':
              // set zero to current position
              x = 0; y = 0; z = 0;
              Serial.println("zeroed");
              reportReady();
              break;
            default:
              Serial.print("ignoring command: ");
              Serial.println(command);
          }
        }
        else {
          //          reportReady();
        }
      }
      break;
    case 'M':
      move();
      break;
  }
  long pause_time = cycle_duration - (millis() - start_time);
  if (millis()-last_notice_time > REPORT_FREQUENCY) {
    report();
        Serial.print(" pause_time: "); Serial.print(pause_time);
  }
  if (weMoved() && pause_time > 0) {
    //    Serial.print(" pauseing: "); Serial.println(pause_time);
    delayMicroseconds(pause_time);
  }
}

/*
   * Read a gcommand from serial
   * sets up delta_? and path_?
   */
void readGCommand() {
  Serial.println("Reading G command");
  target_x = Serial.parseInt() * X_CLICKS_PER_MM;
  target_y = Serial.parseInt() * Y_CLICKS_PER_MM;
  target_z = Serial.parseInt() * Z_CLICKS_PER_MM;
  feed_rate = Serial.parseInt();
  Serial.print(" New target received: ");
  Serial.print(target_x);
  Serial.print(", ");
  Serial.print(target_y);
  Serial.print(", ");
  Serial.println(target_z);
  long x_steps = target_x - x;
  long y_steps = target_y - y;
  long z_steps = target_z - z;
  // what is the most number of steps in any direction:
  move_steps = max(max(abs(x_steps), abs(y_steps)), abs(z_steps));
  delta_x = (float)x_steps / move_steps;
  delta_y = (float)y_steps / move_steps;
  delta_z = (float)z_steps / move_steps;
  // set the float values to our current location
  path_x = x;
  path_y = y;
  path_z = z;
  // wait for end of line then send OK
  while (Serial.read() != '\n');
  report();
  mode = 'M'; // in transit;
}

/*
 * Implement a linear move step in 3 dimensions
 * @uses delta_? to determine rate of movement
 * @uses path_? to track desired position
 * @uses target_? to report final variance (should be 0)
 * @uses move_steps to track the number of steps to go as this part of move
 * sets mode back to 'G' when complete
 */
void move() {
  if (move_steps > 0) {
    //    Serial.print("steps to go:");
    //    Serial.println(move_steps);
    int divergence;
    // calculate (decimal) desired location
    path_x += delta_x;
    path_y += delta_y;
    path_z += delta_z;
    //        Serial.print(" Delta: ");
    //        Serial.print(delta_x);
    //        Serial.print(", ");
    //        Serial.print(delta_y);
    //        Serial.print(", ");
    //        Serial.print(delta_z);
    //        Serial.print(" Path: ");
    //        Serial.print(path_x);
    //        Serial.print(", ");
    //        Serial.print(path_y);
    //        Serial.print(", ");
    //        Serial.print(path_z);
    //        Serial.println();
    // check each dimension and move as needed:
    divergence = path_x - x;
    //    Serial.print(" divergence x "); Serial.print(divergence);
    x += moveAxis(divergence, x, X_PIN);
    divergence = path_y - y;
    //    Serial.print(" divergence y "); Serial.print(divergence);
    y += moveAxis(divergence, y, Y_PIN);
    divergence = path_z - z;
    //    Serial.print(" divergence z "); Serial.print(divergence);
    z += moveAxis(divergence, z, Z_PIN);
    // report every thousand steps:
    if (!(move_steps % 1000)) {
      report();
    }
    move_steps--;
  }
  else {
    // end of move!
    Serial.print("end of move variance: x:"); Serial.print(x-target_x);
    Serial.print(" y:"); Serial.print(y-target_y);
    Serial.print(" z:"); Serial.print(z-target_z);
    Serial.println(".");
    report();
    mode = 'G';
    reportReady();
  }

}

/**
  * Let controller know we are good to receive command
  */
void reportReady() {
  Serial.println("ok");
}

// take a joystick value and move
// retursn
int moveAxisJoy(int joyStick, int current, int pin) {
  int offset = abs(joyStick-512);
  int direction = (joyStick > 512 ? 1 : -1);
  if (offset > 300) {
    return moveAxis(direction, current, pin);
  }
  else {
    return 0;
  }
}

// returns the number of steps take (1 or -1)
int moveAxis(int direction, int current, int pin) {
  int result;
  int direction_pin = pin;
  int step_pin = pin+1;
  if (direction == 0) {
    result = 0;
  }
  else {
    // toggle direction pin if needed
    if ( (direction > 0) != (digitalRead(direction_pin) == HIGH)) {
      digitalWrite(direction_pin, !digitalRead(direction_pin));
    }
    // step:
    digitalWrite(step_pin, !digitalRead(step_pin));
    result = direction > 0 ? 1 : -1;
  }
  //  Serial.print(" moved "); Serial.print(result);
  //  Serial.print(" via pin "); Serial.println(step_pin);
  return result;
}

// test to see if movement has happend
int weMoved() {
  return start_x != x || start_y != y || start_z != z;
}

// alternate reading X & Y
// copes with the fact you cant read analogue values quickly
void update_joy() {
  if (millis() - last_analogue_read_time > MIN_ANALOGUE_READ_GAP) {
    last_analogue_read_time = millis();
    if (last_analogue_read_axis == 'Y') {
      // read the X
      joyX = analogRead(JOYSTICK_X_PIN);
      analogRead(JOYSTICK_Y_PIN); // read the next pin to give time to stabilise
      last_analogue_read_axis = 'X';
    }
    else {
      // read the Y
      if (digitalRead(JOYSTICK_AXIS_SWITCH_PIN)) {
        joyY = analogRead(JOYSTICK_Y_PIN);
        joyZ = 512;
      }
      else {
       joyY = 512;
       joyZ = analogRead(JOYSTICK_Y_PIN);
      }
      analogRead(JOYSTICK_X_PIN);
      last_analogue_read_axis = 'Y';
    }
  }
}

// output a status report
// updates last_notice_time
void report() {
  // todo try using sprintf(var, )
  Serial.print(mode);
  Serial.print(cycle);
  Serial.print("    Head at : ");
  Serial.print(x);
  Serial.print(", ");
  Serial.print(y);
  Serial.print(", ");
  Serial.print(z);
  if (mode == 'J') {
    Serial.print(" Stick at: ");
    Serial.print(joyX);
    Serial.print(", ");
    Serial.print(joyY);
    Serial.print(", ");
    Serial.print(joyZ);
  }
  if (mode == 'M') {
    Serial.print(" Target: ");
    Serial.print(target_x);
    Serial.print(", ");
    Serial.print(target_y);
    Serial.print(", ");
    Serial.print(target_z);
    Serial.print(", Feed: ");
    Serial.print(feed_rate);
  }
  Serial.println();
  last_notice_time = millis();
  digitalWrite(13, !digitalRead(13));
}
