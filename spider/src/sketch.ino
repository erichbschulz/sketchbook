#include <IRremote.h>
#include "pitches.h"
const int PIEZO_PIN = 3;

IRrecv irrecv(12); // Receive on pin 12

unsigned long left_cycle = 0;
unsigned long right_cycle = 0;
unsigned int delay_time = 30;

Servo myServo[8];

#include <Servo.h>
#define FIRST_PIN 4


#define NUMBER_OF_LEGS 4
#define BACK_STEPS 70
#define FORWARD_STEPS 10
#define TOTAL_STEPS 80 // = forward + back
#define FORWARD_POSITION 35 //
#define UP_POSITION -90 //
#define DOWN_POSITION -20 //

#define FORWARD_MODE 1
#define STOPPED_MODE 0
#define BACKWARD_MODE 2
#define LEGS_UP_MODE 3
#define LEFT_TURN_MODE 4
#define RIGHT_TURN_MODE 5

byte mode = LEGS_UP_MODE;

void step() {
  // step through legs (2 servos each)
  for(int i=0; i < NUMBER_OF_LEGS * 2; i=i+2) {
    // -1 for left, 1 for right:
    int side = (i == 0 || i == 6 ) ? -1 : 1;
    byte phase = ((side == 1 ? right_cycle : left_cycle) + i * 10) % TOTAL_STEPS;
    // -1 for clockwise up, 1 for clockwise down
    int mounting = (i == 0 || i == 4 ) ? -1 : 1;
    // up or down
    if (phase > BACK_STEPS) {
      // move leg up and forward
      // up
      position(i, UP_POSITION, mounting);
      // forward
      position(i+1, FORWARD_POSITION, side);
    }
    else {
      // go neutral (down)
      position(i, DOWN_POSITION, mounting);
      // slowly back with cycle
      position(i+1, FORWARD_POSITION - phase, side);
    }
  }
}

void legs_up() {
  // step through legs (2 servos each)
  for(int i=0; i < NUMBER_OF_LEGS * 2; i=i+2) {
    // -1 for clockwise up, 1 for clockwise down
    int mounting = (i == 0 || i == 4 ) ? -1 : 1;
    position(i, UP_POSITION, mounting);
  }
}

void position(byte servo, int angle, int direction) {
  if (servo == 1) {
    // fudge crooked leg!
    angle -= 30;
  }
  myServo[servo].write(90 + angle * direction);
}

void setup() {
  for(int i=0; i<8; i++) {
    myServo[i].attach(i+FIRST_PIN);
    myServo[i].write(90);
  }
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the IR receiver
  Serial.println("starting");
  report();
  reportReady();
}

void loop() {
  mode = check_ir();
  switch (mode) {
    case STOPPED_MODE:
      break;
    case LEGS_UP_MODE:
      legs_up();
      break;
    case FORWARD_MODE:
      left_cycle ++;
      right_cycle ++;
      step();
      break;
    case BACKWARD_MODE:
      left_cycle --;
      right_cycle --;
      step();
      break;
    case LEFT_TURN_MODE:
      left_cycle --;
      right_cycle ++;
      step();
      break;
    case RIGHT_TURN_MODE:
      left_cycle ++;
      right_cycle --;
      step();
      break;
  }
  delay(delay_time);
  report();
}

// output a status report
// updates last_notice_time
byte check_ir() {
  decode_results results;
  unsigned long button;
  byte new_mode = mode;
  if (irrecv.decode(&results)) {
    button = results.value;
    irrecv.resume(); // Continue receiving
    switch(button) {
      case 0xFFA25D:
        new_mode = STOPPED_MODE;
        Serial.print(" CH-            ");
        break;
      case 0xFF629D:
        new_mode = FORWARD_MODE;
        Serial.print(" CH             ");
        break;
      case 0xFFE21D:
        new_mode = BACKWARD_MODE;
        Serial.print(" CH+            ");
        break;
      case 0xFF22DD:
        new_mode = LEFT_TURN_MODE;
        Serial.print(" PREV           ");
        break;
      case 0xFF02FD:
        new_mode = RIGHT_TURN_MODE;
        Serial.print(" NEXT           ");
        break;
      case 0xFFC23D:
        Serial.print(" PLAY/PAUSE     ");
        break;
      case 0xFFE01F:
        Serial.print(" VOL-           ");
        delay_time -= 10;
        Serial.print(delay_time);
        break;
      case 0xFFA857:
        Serial.print(" VOL+           ");
        delay_time += 10;
        Serial.print(delay_time);
        break;
      case 0xFF906F:
        song();
        Serial.print(" EQ             ");
        break;
      case 0xFF6897:
        Serial.print(" 0              ");
        break;
      case 0xFF9867:
        Serial.print(" 100+           ");
        break;
      case 0xFFB04F:
        Serial.print(" 200+           ");
        break;
      case 0xFF30CF:
        new_mode = LEGS_UP_MODE;
        Serial.print(" 1              ");
        break;
      case 0xFF18E7:
        Serial.print(" 2              ");
        break;
      case 0xFF7A85:
        Serial.print(" 3              ");
        break;
      case 0xFF10EF:
        Serial.print(" 4              ");
        break;
      case 0xFF38C7:
        Serial.print(" 5              ");
        break;
      case 0xFF5AA5:
        Serial.print(" 6              ");
        break;
      case 0xFF42BD:
        Serial.print(" 7              ");
        break;
      case 0xFF4AB5:
        Serial.print(" 8              ");
        break;
      case 0xFF52AD:
        Serial.print(" 9              ");
        break;
      case 0xFFFFFFFF:
        Serial.print(" REPEAT         ");
        break;
      default:
        Serial.print(" other button   ");
    }
    Serial.println(button, HEX);
  }
  return new_mode;
}

// output a status report
// updates last_notice_time
void report() {
}

/**
  * Let controller know we are good to receive command
  */
void reportReady() {
  Serial.println("ok");
}

/**
  * Play song
  */
void song() {
  int melody[] = {NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_G4, NOTE_B5, NOTE_C5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_C5, NOTE_B5, NOTE_A5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_G4, NOTE_B5, NOTE_C5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_C5, NOTE_B5, NOTE_A5, NOTE_B5, NOTE_C5, NOTE_D5, NOTE_E5, NOTE_G4, NOTE_F5, NOTE_E5, NOTE_D5, NOTE_F4, NOTE_E5, NOTE_D5, NOTE_C5, NOTE_E4, NOTE_D5, NOTE_C5, NOTE_B5, NOTE_E4, NOTE_E5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_G4, NOTE_B5, NOTE_C5, NOTE_E4, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_D5, NOTE_E5, NOTE_B5, NOTE_D5, NOTE_C5, NOTE_A5, NOTE_C4, NOTE_E4, NOTE_A5, NOTE_B5, NOTE_E4, NOTE_C5, NOTE_B5, NOTE_A3 };
  int beats[] = { 1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,2,1,1,1,2,1,1,1,2,1,1,1,3,1,5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,3,1,1,1,1,1,1,1,1,1,3,1,1,1,3,1,1,1,6};
  int tempo = 175;
  int note_count = sizeof(melody);
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < note_count; thisNote++) {
    int noteDuration = beats[thisNote] * tempo;
    tone(PIEZO_PIN, melody[thisNote], noteDuration);
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    noTone(PIEZO_PIN);
  }

}
